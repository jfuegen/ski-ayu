extends Node2D

class_name Pile

signal clicked

@onready var cards := $Cards
@onready var glow := $Glow

var ready_for_action := false :
	set(value):
		ready_for_action = value
		glow.visible = value
		if not ready_for_action:
			Input.set_default_cursor_shape(Input.CURSOR_ARROW)

func add_cards(new_cards : Array[Card], face_visible = true) -> void:
	for card in new_cards:
		cards.add_child(card)
		card.face_visible = face_visible
	
func add_card(new_card : Card, face_visible = true, ignore_null=true) -> void:
	if ignore_null and new_card == null:
		return
	#print("New card added to pile: ", new_card)
	if new_card.get_parent():
		new_card.get_parent().remove_child(new_card)
	cards.add_child(new_card)
	new_card.position = Vector2(0,0)
	new_card.face_visible = face_visible
	
func shuffle() -> void:
	var children = cards.get_children()
	children.shuffle()
	for child in children:
		cards.remove_child(child)
		cards.add_child(child)

func move_top_card_to_pile(pile: Pile) -> void:
	var tween = create_tween()
	var card = get_top_card()
	# Move the card immediately so that it does not interfere with game logic
	# checking for empty or the new top card. 
	cards.remove_child(card)
	pile.add_card(card, card.face_visible)
	card.global_position = global_position
	tween.tween_property(card, "global_position", pile.global_position, 0.2)
	tween.tween_property(card, "position", Vector2(0,0), 0)
	await tween.finished

func get_top_card() -> Card:
	return cards.get_child(-1)

func is_empty() -> bool:
	return cards.get_child_count() == 0
	
func is_card_visible() -> bool:
	if is_empty():
		return false
	return get_top_card().face_visible

func clear() -> void:
	for card in cards.get_children():
		cards.remove_child(card)
		card.queue_free()
	
func get_top_value(only_visible := true, default := 0) -> int:
	if is_empty() or (not get_top_card().face_visible and only_visible):
		return default
	return cards.get_child(-1).value


func _on_area_2d_mouse_entered():
	if ready_for_action:
		Input.set_default_cursor_shape(Input.CURSOR_POINTING_HAND)


func _on_area_2d_mouse_exited():
	if ready_for_action:
		Input.set_default_cursor_shape(Input.CURSOR_ARROW)


func _on_area_2d_input_event(_viewport, event : InputEvent, _shape_idx):
	if ready_for_action and event.is_action_pressed("click"):
		clicked.emit(self)

func _to_string():
	return "<Pile, Top Card: %s (visible: %s)>" % [get_top_value(), get_top_card().face_visible ]

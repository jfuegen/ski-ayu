extends Node2D

class_name PileGrid

signal pile_clicked

const PileScene := preload("res://scenes/pile.tscn")

@export var grid_size := Vector2i(4, 3)
@export var card_size := Vector2i(80, 120)
@export var gap := 20


@onready var piles = $Piles

# Called when the node enters the scene tree for the first time.
func _ready():
	_initialize()

func add_cards(cards : Array[Card], face_visible = true) -> void:
	for i in range(min(piles.get_child_count(), len(cards))):
		add_card(cards[i], i, face_visible)

func add_card(card: Card, idx: int, face_visible = true):
	piles.get_child(idx).add_card(card)
	card.face_visible = face_visible


func set_all_piles_ready_for_action(ready_for_action : bool, empty := true, open := true, closed := true) -> void:
	for pile : Pile in piles.get_children():
		if not empty and pile.is_empty():
			continue
		if (open and pile.is_card_visible()) or (closed and not pile.is_card_visible()):
			set_pile_ready_for_action(pile, ready_for_action)


func set_pile_ready_for_action(pile : Pile, ready_for_action : bool) -> void:
	pile.ready_for_action = ready_for_action
	if ready_for_action:
		if not pile.is_connected("clicked", _on_pile_clicked):
			pile.connect("clicked", _on_pile_clicked)
	else:
		if pile.is_connected("clicked", _on_pile_clicked):
			pile.disconnect("clicked", _on_pile_clicked)


func _initialize():
	for pile in piles.get_children():
		piles.remove_child(pile)
	for y in range(grid_size.y):
		for x in range(grid_size.x):
			var pile = PileScene.instantiate()
			
			piles.add_child(pile)
			pile.position = Vector2i(x,y) * (card_size + Vector2i(gap, gap))
			pile.ready_for_action = false


func get_pile(x: int, y: int = -1) -> Pile:
	if y == -1:
		return piles.get_child(x)
	#print(x, "/", y, ": ", piles.get_child(y * grid_size.x + x).get_top_value())
	return piles.get_child(y * grid_size.x + x)


func get_row(y: int) -> Array[Pile]:
	var row : Array[Pile] = []
	for x in range(grid_size.x):
		row.append(get_pile(x, y))
	return row
	
	
func get_col(x: int) -> Array[Pile]:
	var col : Array[Pile] = []
	for y in range(grid_size.y):
		col.append(get_pile(x, y))
	#print(x, " col: ", col)
	return col


func clear() -> void:
	# Complete re-setup
	_initialize()


func _on_pile_clicked(pile: Pile):
	pile_clicked.emit(pile)

extends Node

class_name Player

@onready var score_label = $ScoreLabel
@onready var grid : PileGrid= $Grid
@onready var all_scores = $AllScores

var scores : Array[int] = []
var current_score : int = 0:
	set(value):
		current_score = value
		if score_label:
			score_label.text = str(value)
var first_round := true

func add_score(value : int) -> void:
	scores.append(value)
	all_scores.text = "%s = %s" % [" + ".join(scores), get_overall_score()]

func clear_scores() -> void:
	scores.clear()
	all_scores.text = ""

func get_overall_score() -> int:
	var sum := 0
	for score in scores:
		sum += score
	return sum
		
func reset_round() -> void:
	current_score = 0
	first_round = true
	grid.clear()

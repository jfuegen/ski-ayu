extends State

@onready var game : Game = $"../.."
@onready var finished = $"../Finished"
@onready var select_pile = $"../SelectPile"
@onready var open_2_cards = $"../Open2Cards"



func _enter_state():
	
	# Check if we have columns to remove
	for x in range(game.current_player.grid.grid_size.x):
		var all_same = true
		var val = -1000 # some non-existent value
		for pile : Pile in game.current_player.grid.get_col(x):
			if not all_same or pile.is_empty() or not pile.is_card_visible():
				all_same = false
				continue
			if val == -1000:
				val = pile.get_top_value()
			else:
				if val != pile.get_top_value():
					all_same = false
					continue
		if all_same:
			for pile : Pile in game.current_player.grid.get_col(x):
				await pile.move_top_card_to_pile(game.open_pile)
	# Check, if we have all cards uncovered.
	# Also count the current open cards.
	var all_uncovered = true
	var count = 0
	for pile : Pile in game.current_player.grid.piles.get_children():
		if not pile.is_empty() and not pile.is_card_visible():
			all_uncovered = false
		else:
			count += pile.get_top_value()
	game.current_player.current_score = count
		
	# The round is finished, when all cards are uncovered
	if all_uncovered:
		for player in [game.player_1, game.player_2]:
			if player == game.current_player:
				continue
			# Uncover all cards of the other player and also count
			count = 0
			for pile: Pile in player.grid.piles.get_children():
				if not pile.is_empty() and not pile.is_card_visible():
					await pile.get_top_card().turn_card()
				count += pile.get_top_value(false)
			if player == game.player_1:
				game.player_1.current_score = count
			else:
				game.player_2.current_score = count
		get_parent().next(finished)
	else:
		# Check if we have to let the second player open cards
		if game.get_other_player().first_round:
			switch_player()
			get_parent().next(open_2_cards)
		else:
			if get_parent().last_state == open_2_cards:
				# Only in first round:
				if len(game.current_player.scores) == 0:
					# Check if current player has higher count to determin who starts
					if game.player_1.current_score >= game.player_2.current_score:
						switch_to_player(game.player_1)
					else:
						switch_to_player(game.player_2)
				else:
					# If not in first round, but after card opening, 
					# we switch back to the finishing player of last round
					switch_player()
			else:
				# All other rounds, just switch player
				switch_player()
			get_parent().next(select_pile)
				

func _exit_state():
	pass
	
func switch_to_player(player : Player):
	if game.current_player != player:
		game.current_player = player
		var tween = create_tween()
		tween.tween_property(game.arrow, "rotation_degrees", (game.arrow.rotation_degrees as int + 180) % 360, 0.5).set_trans(Tween.TRANS_BACK).set_ease(Tween.EASE_IN_OUT)
	
func switch_player():
		# Also very ugly change of the player...
	if game.current_player == game.player_1:
		switch_to_player(game.player_2)
	else:
		switch_to_player(game.player_1)

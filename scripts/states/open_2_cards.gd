extends State

@onready var game : Game = $"../.."
@onready var select_pile = $"../SelectPile"
@onready var select_grid = $"../SelectGrid"
@onready var finished = $"../Finished"
@onready var end_turn = $"../EndTurn"

var cards_chosen := 0

func _enter_state():
	cards_chosen = 0
	game.current_player.grid.set_all_piles_ready_for_action(true, false, false)
	game.current_player.grid.connect("pile_clicked", _grid_clicked)

func _exit_state():
	game.current_player.grid.set_all_piles_ready_for_action(false)
	game.current_player.grid.disconnect("pile_clicked", _grid_clicked)
	
func _grid_clicked(pile: Pile):
	await pile.get_top_card().turn_card()
	pile.ready_for_action = false
	cards_chosen += 1
	if cards_chosen == 2:
		game.current_player.first_round = false
		get_parent().next(end_turn)


extends State

@onready var game : Game = $"../.."
@onready var select_pile = $"../SelectPile"
@onready var select_grid = $"../SelectGrid"
@onready var finished = $"../Finished"
@onready var end_turn = $"../EndTurn"


func _enter_state():
	# When the active card is empty, we discarded it and therefore a closed pile has to be opened
	game.current_player.grid.set_all_piles_ready_for_action(true, false, not game.active_card.is_empty())
	game.current_player.grid.connect("pile_clicked", _grid_clicked)

func _exit_state():
	game.current_player.grid.set_all_piles_ready_for_action(false)
	game.current_player.grid.disconnect("pile_clicked", _grid_clicked)
	
func _grid_clicked(pile: Pile):
	if game.active_card.is_empty():
		await pile.get_top_card().turn_card()
	else:
		if not pile.is_card_visible():
			await pile.get_top_card().turn_card()
			await pile.move_top_card_to_pile(game.open_pile)
		else:
			await pile.move_top_card_to_pile(game.open_pile)
		await game.active_card.move_top_card_to_pile(pile)
		
	get_parent().next(end_turn)

